package servidorRMI;

import java.rmi.RemoteException;

public class Main {
	private static Server server;
	private static String serverName = "RMI";
	private static int serverPort = 5555;

	private static Client client;
	private static String serverIP = "localhost";

	public static void main(String[] args) throws RemoteException {
		server = new Server();
		server.NewServerImplementacion(serverName, serverPort);

		 client = new Client();
		 client.NewServerConnection(serverIP, serverName, serverPort);
		 client.initialize();
	}
}
