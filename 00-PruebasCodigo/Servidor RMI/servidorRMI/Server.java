package servidorRMI;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server implements ServerInterface {

	/**
	 * Este metodo sirve para crear un nuevo servidor RMI que escuchara en el
	 * puerto designado y tendra el identificador indicado. SOLO SE DEBE CREAR
	 * UNA VEZ.
	 * 
	 * @param serverName
	 *            (String) Nombre del servidor. Es el que buscaran los clientes.
	 * @param serverPort
	 *            (int) Puerto en el que escuchara el servidor.
	 */
	public void NewServerImplementacion(String serverName, int serverPort) {
		Registry reg = null;

		try {
			// Creamos el registro de objetos, que escuchara en el puero
			// serverPort.
			reg = LocateRegistry.createRegistry(serverPort);
		} catch (Exception e) {
			System.out.println("ERROR: No se ha podido crear el registro");
			e.printStackTrace();
		}

		try {
			// Incluimos el objeto serverObject creado en el registro y le damos
			// un nombre identificativo.F
			reg.rebind(serverName, UnicastRemoteObject.exportObject(this, 0));
		} catch (Exception e) {
			System.out.println("ERROR: No se ha podido inscribir el objeto servidor.");
			e.printStackTrace();
		}
	}

	@Override
	public String ShowText(String text) {
		return (text + " - Servidor OK");
	}
}
