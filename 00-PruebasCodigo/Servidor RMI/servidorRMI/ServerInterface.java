package servidorRMI;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServerInterface extends Remote {

	/**
	 * Este metodo sirve para confirmar que se ha establecido una conexión con
	 * el servidor.
	 * 
	 * @param text
	 *            (String) Cadena de texto enviada por el cliente.
	 * @return (String) Devuelve el texto recibido (text) seguido de
	 *         " - Servidor OK".
	 */
	public String ShowText(String text) throws RemoteException;
}
