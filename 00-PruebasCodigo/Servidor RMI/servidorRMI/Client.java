package servidorRMI;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client {
	private ServerInterface server;

	/**
	 * Este metodo sirve para buscar y establecer una conexion con el servidor
	 * indicado en la ip y puerto designados.
	 * 
	 * @param serverIP
	 *            (String) Direccion IP donde se encuentra alojado el servidor.
	 * @param serverName
	 *            (String) Nombre (ID) del servidor al que se quiere conectar.
	 * @param serverPort
	 *            (int) Puerto donde esta escuchando el servidor.
	 */
	public void NewServerConnection(String serverIP, String serverName, int serverPort) {
		server = null;

		try {
			// Localiza los objetos remotos en la ip y el puerto especificados
			Registry registry = LocateRegistry.getRegistry(serverIP, serverPort);
			// El objeto stub 'servidor' se utiliza para invocar los metodos del
			// servidor de manera remota
			server = (ServerInterface) registry.lookup(serverName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void initialize() throws RemoteException{
		initializeContent();
	}
	
	private void initializeContent() throws RemoteException{
		System.out.println("Se va a realizar una petición al server.");
		System.out.println(server.ShowText("Peticion al server"));
	}
}
