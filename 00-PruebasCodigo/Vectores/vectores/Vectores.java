package vectores;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Vector;

class Metodos {
	short leerTeclado() {
		short temp = 0;
		String read = "";
		try {
			BufferedReader entrada = new BufferedReader(new InputStreamReader(
					System.in));
			read = entrada.readLine();
		} catch (Exception e) {
			System.err.println("\n\n���ATENCI�N!!!"
					+ "\n   Se ha producido un error en la lectura de datos.");
		}
		temp = Short.valueOf(read);
		return temp;
	}
}

public class Vectores {
	public static void main(String[] args) {
		Metodos m = new Metodos();
		Vector<Short> v = new Vector<Short>(5, 1);
		short n;

		System.out.print("Valores a introducir en el vector: ");
		n = m.leerTeclado();

		for (short i = 0; i < n; i++) {
			System.out.print("Introduce el "+(i+1)+"� n�mero: ");
			v.addElement(m.leerTeclado());
			System.err.println(v.elementAt(i)+"\t");
		}

	}
}
