package ficheros;

import java.util.Scanner;

public class Main {
	public static void main(String args[]) {
		MetodosFicheros mf = new MetodosFicheros();
		String rutaFichero = "";

		Scanner scan = new Scanner(System.in);
		System.out.print("Introduce la ruta del Fichero: ");
		rutaFichero = scan.nextLine();

		mf.escribirFichero(rutaFichero);
		mf.leerFichero(rutaFichero);

		scan.close();
	}
}
