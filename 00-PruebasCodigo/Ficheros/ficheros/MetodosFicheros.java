package ficheros;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;

public class MetodosFicheros {

	/**
	 * Este metodo leera y mostrara por pantalla todo el contenido almacenado en
	 * el fichero (.txt, .bat) especificado. ATENCION: Si no existe el fichero
	 * especificado, se lanzara una excepci�n.
	 * 
	 * @param rutaFichero
	 *            Ruta en la que se encuentra el fichero.
	 */
	public void leerFichero(String rutaFichero) {
		File fichero = null;
		FileReader fr = null;
		BufferedReader br = null;
		try {
			fichero = new File(rutaFichero);
			fr = new FileReader(fichero);
			br = new BufferedReader(fr);

			String linea;
			System.out.println("   Contenido del Fichero:");
			// Lee hasta que no haya mas lineas.
			while ((linea = br.readLine()) != null) {
				// Devuelve el valor de cada linea.
				System.out.println(linea);
			}
		} catch (Exception e) {
			System.err
					.println("Se ha producido un error al leer el fichero o este no existe.");
		} finally {
			try {
				if (null != fr) {
					fr.close();
				}
			} catch (Exception e) {
				System.err
						.println("Se ha producido un error al cerrar el fichero.");
			}
		}
	}

	/**
	 * Este metodo leera y almacenara en un variable el contenido almacenado en
	 * el fichero (.txt, .bat) especificado. Al terminar, devolvera dicha
	 * variable (String). ATENCION: Si no existe el fichero especificado, se
	 * lanzara una excepci�n.
	 * 
	 * @param rutaFichero
	 *            Ruta en la que se encuentra el fichero.
	 * @return String Contenido del fichero.
	 */
	public String leerFichero2(String rutaFichero) {
		File fichero = null;
		FileReader fr = null;
		BufferedReader br = null;
		String txt = " ", temp = " ";

		try {
			fichero = new File(rutaFichero);
			fr = new FileReader(fichero);
			br = new BufferedReader(fr);

			// Lee hasta que no haya mas lineas.
			while ((temp = br.readLine()) != null) {
				// Almacena el contenido de cada linea en txt, para luego
				// devolver el contenido del fichero.
				txt = temp;

			}
		} catch (Exception e) {
			System.err
					.println("Se ha producido un error al leer el fichero o este no existe.");
		} finally {
			try {
				if (null != fr) {
					fr.close();
				}
			} catch (Exception e) {
				System.err
						.println("Se ha producido un error al cerrar el fichero.");
			}
		}
		return txt;
	}

	/**
	 * Este metodo recibe contenido desde fuera del metodo y lo guarda en el
	 * fichero (.txt, .bat) especificado. ATENCION: Si hay contenido previo en
	 * el fichero sera eliminado.
	 * 
	 * @param rutaFichero
	 *            Ruta en la que se guardara el fichero.
	 * @param contenido
	 *            Contenido a guardar en el fichero.
	 */
	public void escribirFichero(String rutaFichero, String contenido) {
		FileWriter fichero = null;
		PrintWriter pw = null;
		try {
			fichero = new FileWriter(rutaFichero);
			pw = new PrintWriter(fichero);
			pw.println(contenido);
		} catch (Exception e) {
			System.err
					.println("Se ha producido un error al escribir en el fichero.");
		} finally {
			try {
				if (null != fichero) {
					fichero.close();
				}
			} catch (Exception e) {
				System.err
						.println("Se ha producido un error al cerrar el fichero.");
			}
		}
	}

	/**
	 * Este metodo pide por teclado y lo va guardando en un fichero (.txt, .bat)
	 * hasta que se introduce 0 (marca el fin de archivo). ATENCION: Si hay
	 * contenido previo en el fichero sera eliminado.
	 * 
	 * @param rutaFichero
	 *            Ruta en la que se guardara el fichero.
	 */
	public void escribirFichero(String rutaFichero) {
		Scanner scan = new Scanner(System.in);
		FileWriter fichero = null;
		PrintWriter pw = null;
		String temp = "";
		try {
			fichero = new FileWriter(rutaFichero);
			pw = new PrintWriter(fichero);

			System.out.println("   Introduce el texto a guardar:");
			do {
				temp = scan.nextLine();
				if (!(temp.equals("0"))) {
					pw.println(temp);
				}
			} while (!(temp.equals("0")));
			scan.close();
		} catch (Exception e) {
			System.err
					.println("Se ha producido un error al escribir en el fichero.");
		} finally {
			try {
				if (null != fichero) {
					fichero.close();
				}
			} catch (Exception e) {
				System.err
						.println("Se ha producido un error al cerrar el fichero.");
			}
		}
	}
}
