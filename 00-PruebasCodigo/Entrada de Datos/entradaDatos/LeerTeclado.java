package entradaDatos;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class LeerTeclado {
	/**
	 * @author David Navarro de la Morena
	 * 
	 * Este metodo procesa todo el contenido que se introduce por teclado, lo
	 * almacena en una variable y lo devuelve en forma de String.
	 * 
	 * @return String Devuelve el contenido introducido por teclado.
	 */
	String leerTecladoString() {
		String read = "";

		try {
			BufferedReader entrada = new BufferedReader(new InputStreamReader(
					System.in));
			read = entrada.readLine();
		} catch (Exception e) {
			System.err
					.println("\nHas introducido un valor no valido. Intentalo de nuevo.\n");
			read = leerTecladoString();
		}

		return read;
	}

	/**
	 * @author David Navarro de la Morena
	 * 
	 * Este metodo procesa todo el contenido que se introduce por teclado, lo
	 * almacena en una variable y lo devuelve en forma de int.
	 * 
	 * @return int Devuelve el contenido introducido por teclado.
	 */
	int leerTecladoInt() {
		int temp = 0;
		String read = "";

		try {
			BufferedReader entrada = new BufferedReader(new InputStreamReader(
					System.in));
			read = entrada.readLine();
			temp = Integer.parseInt(read);
		} catch (Exception e) {
			System.err
					.println("\nHas introducido un valor no valido. Intentalo de nuevo.\n");
			temp = leerTecladoInt();
		}

		return temp;
	}
}
