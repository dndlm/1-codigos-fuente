package arrayList;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

class Metodos {
	int leerTeclado() {
		int temp = 0;
		String read = "";
		try {
			BufferedReader entrada = new BufferedReader(new InputStreamReader(
					System.in));
			read = entrada.readLine();
		} catch (Exception e) {
			System.err.println("\n\n���ATENCI�N!!!"
					+ "\n   Se ha producido un error en la lectura de datos.");
		}
		temp = Integer.parseInt(read);
		return temp;
	}
}

public class ArrayLista {
	public static void main(String[] args) {
		Metodos m = new Metodos();
		/*
		 * Declaramos el ArrayList (por defecto admite cualquier valor, incluso
		 * mezclados) y le damos el tipo int (hay que poner el envoltorio para
		 * que lo entienda; Integer en este caso). Se le puede poner el tipo que
		 * queramos o no poenr ninguno si queremos usar varios tipos, pero en
		 * ese caso los convierte a la clase envoltorio (un int lo convertiria a
		 * Integer, un short a Short, etc.
		 */
		ArrayList<Integer> a = new ArrayList<Integer>();
		int n = 0;

		/*
		 * <<--------->> METODOS DEL ArrayList <<---------->>
		 * 
		 * size() Devuelve el n�mero de elementos (int)
		 * 
		 * add(X) A�ade el objeto X al final. Devuelve true.
		 * 
		 * add(posici�n, X) Inserta el objeto X en la posici�n indicada.
		 * 
		 * get(posicion) Devuelve el elemento que est� en la posici�n indicada.
		 * 
		 * remove(posicion) Elimina el elemento que se encuentra en la posici�n
		 * indicada. Devuelve el elemento eliminado.
		 * 
		 * remove(X) Elimina la primera ocurrencia del objeto X. Devuelve true
		 * si el elemento est� en la lista.
		 * 
		 * clear() Elimina todos los elementos.
		 * 
		 * set(posici�n, X) Sustituye el elemento que se encuentra en la
		 * posici�n indicada por el objeto X. Devuelve el elemento sustituido.
		 * 
		 * contains(X) Comprueba si la colecci�n contiene al objeto X. Devuelve
		 * true o false.
		 * 
		 * indexOf(X) Devuelve la posici�n del objeto X. Si no existe devuelve
		 * -1
		 */

		do {
			System.out.print("Introduce un n�mero: ");
			n = m.leerTeclado();
			if (n != 0)
				a.add(n);
			System.err.println("   El ArrayList tiene " + a.size()
					+ " valores.");
		} while (n != 0);
	}
}
