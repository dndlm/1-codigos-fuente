package servidorMultihilos;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Esta clase, del tipo Thread, es la segunda de dos clases necesarias para
 * montar un servidor capaz de atender, de forma simultanea, varias peciones de
 * clientes.
 * 
 * @author David Navarro de la Morena
 * @version 1.0 - 14/01/2016
 *
 */
public class Server_Thread extends Thread {
	private ServerSocket serverSocket = null;
	private InetSocketAddress addr = null;
	private Socket socket = null;
	private InputStream is = null;
	private OutputStream os = null;

	private int thread_id;

	/**
	 * Constructor de la clase.
	 * 
	 * @param socket
	 *            (Socket) Socket del hilo.
	 * @param thread_id
	 *            (int) Identificador del hilo.
	 */
	public Server_Thread(Socket socket, int thread_id) {
		this.socket = socket;
		this.thread_id = thread_id;
	}

	@Override
	public void run() {
		initialize();
	}

	private void initialize() {
		try {
			is = socket.getInputStream();
			os = socket.getOutputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String mensajeTemp = "";

		do {
			mensajeTemp = reciveData(is);
			if (!mensajeTemp.equalsIgnoreCase("fin")) {
				/*
				 * Si el texto recibido es distinto de FIN, se ejecutaran las
				 * lineas contenidas en este punto.
				 */

				// La siguiente linea, enviara un mensaje con la respuesta
				// generada. respuesta al cliente.
				mensajeTemp = sendData(os, mensajeTemp);
			}
		} while (!mensajeTemp.equalsIgnoreCase("fin"));

		disconnect();
	}

	/**
	 * Este metodo sirve para leer el texto recibido por el servidor.
	 * 
	 * @param is
	 *            (InputStream)
	 * @return (String) El texto (decodificado) recibido por el servidor.
	 */
	private String reciveData(InputStream is) {
		String response = "ERROR";
		byte[] mensaje = new byte[25];

		try {
			is.read(mensaje);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		/**
		 * La linea contenida en este try{}catch{} se encarga de convertir el
		 * array de bytes a un string en formato UTF-8 y elimina toda la
		 * "basura" del mismo usando trim().
		 * 
		 * Fuente original:
		 * http://stackoverflow.com/questions/8007352/how-to-detect-end-of-
		 * string-in-byte-array-to-string-conversion (Tercera respuesta).
		 */
		try {
			response = new String(mensaje, "UTF-8").trim();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * Este metodo se encarga de enviar una respuesta al cliente que esta
	 * conectado a este thread.
	 * 
	 * @param os
	 *            (OutputStream)
	 * @param txt
	 *            (String) Respuesta que se enviara al cliente.
	 * @return (String) Se devuelve el texto de respuesta (txt). Si es igual a
	 *         FIN, terminara la ejecucion de este hilo.
	 */
	private String sendData(OutputStream os, String txt) {
		try {
			os.write(txt.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return txt;
	}

	/**
	 * Este metodo sirve para cerrar el socket desde el que se llama.
	 */
	private void disconnect() {
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//
	// <-------------------->_INSERTAR_FUNCIONES_DEL_SERVIDOR_<-------------------->
	//
}
