package servidorMultihilos;

public class Main {
	private static Thread server;
	private static String serverIP = "localhost";
	private static int serverPort = 5555;

	private static Thread client;

	public static void main(String[] args) {
		server = new Server(serverIP, serverPort);
		client = new Client(serverIP, serverPort);

		server.start();
		client.start();

		try {
			server.join();
			client.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("\n\n   Servidor desconecatdo.");
		System.out.println("\n\n   Clientes desconecatdo.");
	}
}
