package servidorMultihilos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Esta clase, del tipo Thread, sirve para crear un cliente capaz de conectarse
 * al servidor que contiene este paquete.
 * 
 * @author David Navarro de la Morena
 * @version 1.0 - 14/01/2016
 *
 */
public class Client extends Thread {
	private String serverIP;
	private int serverPort;

	/**
	 * Constructor de la clase Client. Necesita la direccion IP y el puerto
	 * donde se encuentra alojado al servidor y a donde se han de conectar los
	 * clientes.
	 * 
	 * @param serverIP
	 *            (String) IP del servidor al que nos queremos conectar. Ej:
	 *            localhost, 127.0.0.1, 192.168.1.XXX...
	 * @param serverPort
	 *            (int) Puerto de la maquina en que se aloja el servidor.
	 */
	public Client(String serverIP, int serverPort) {
		// this.clientIP = "localhost";
		// this.clientPort = 5555;
		this.serverIP = serverIP;
		this.serverPort = serverPort;
	}

	@Override
	public void run() {
		initialize();
	}

	private void initialize() {
		Socket clientSocket = new Socket();
		InetSocketAddress addr = new InetSocketAddress(serverIP, serverPort);

		OutputStream os = null;
		InputStream is = null;

		try {
			clientSocket.connect(addr);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			os = clientSocket.getOutputStream();
			is = clientSocket.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String mensajeTemp = "";

		do {
			System.out.print("Numero: ");
			mensajeTemp = sendData(os);
			if (!mensajeTemp.equalsIgnoreCase("fin")) {
				mensajeTemp = reciveData(is);
				System.out.println("Premio: " + mensajeTemp);
			} else {
				sendDataFin(os);
			}
		} while (!mensajeTemp.equalsIgnoreCase("fin"));
	}

	/**
	 * Este metodo sirve para enviar al servidor una cadena de texto que se le
	 * pedira al usuario en el propio metodo (usando como auxiliar el metodo
	 * leerTecladoString()).
	 * 
	 * @param os
	 *            (OutputStream)
	 * @return (String) El texto introducido por el usuario.
	 */
	private String sendData(OutputStream os) {
		String mensaje = leerTecladoString();

		try {
			os.write(mensaje.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return mensaje;
	}

	/**
	 * Este metodo sirve para enviar el texto "fin" al servidor el cual, al
	 * recibir el mismo, procedera a apagarse.
	 * 
	 * @param os
	 *            (OutputStream)
	 */
	private void sendDataFin(OutputStream os) {
		String mensaje = "fin";

		try {
			os.write(mensaje.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Este metodo sirve para leer el texto recibido por el servidor.
	 * 
	 * @param is
	 *            (InputStream)
	 * @return (String) El texto (decodificado) recibido por el servidor.
	 */
	private String reciveData(InputStream is) {
		String response = "ERROR";
		byte[] mensaje = new byte[25];

		try {
			is.read(mensaje);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		/**
		 * La linea contenida en este try{}catch{} se encarga de convertir el
		 * array de bytes a un string en formato UTF-8 y elimina toda la
		 * "basura" del mismo usando trim().
		 * 
		 * Fuente original:
		 * http://stackoverflow.com/questions/8007352/how-to-detect-end-of-
		 * string-in-byte-array-to-string-conversion (Tercera respuesta).
		 */
		try {
			response = new String(mensaje, "UTF-8").trim();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * @author David Navarro de la Morena
	 * 
	 *         Este metodo procesa todo el contenido que se introduce por
	 *         teclado, lo almacena en una variable y lo devuelve en forma de
	 *         String.
	 * 
	 * @return (String) Devuelve el contenido introducido por teclado.
	 */

	private String leerTecladoString() {
		String read = "";

		try {
			BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));
			read = entrada.readLine();
		} catch (Exception e) {
			System.err.println("\nHas introducido un valor no valido. Intentalo de nuevo.\n");
			read = leerTecladoString();
		}

		return read;
	}
}