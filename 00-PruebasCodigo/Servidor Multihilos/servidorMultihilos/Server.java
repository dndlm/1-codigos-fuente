package servidorMultihilos;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Esta clase, del tipo Thread, es la primera de dos clases necesarias para
 * montar un servidor capaz de atender, de forma simultanea, varias peciones de
 * clientes.
 * 
 * @author David Navarro de la Morena
 * @version 1.0 - 14/01/2016
 *
 */
public class Server extends Thread {
	private Server_Thread server_thread;

	private String serverIP;
	private int serverPort;

	private static ArrayList<Server_Thread> threads;
	private static int threads_closed;

	/**
	 * Constructor de la clase Server. Necesita una direcci�n IP y un puerto
	 * libre para alojarse y, al cual, se conectaran los clientes.
	 * 
	 * @param serverIP
	 *            (String) IP del servidor. Ej: localhost, 127.0.0.1,
	 *            192.168.1.XXX...
	 * @param serverPort
	 *            (int) Puerto de la maquina en que se alojara el servidor.
	 *            Tiene que estar libre.
	 */
	public Server(String serverIP, int serverPort) {
		// this.serverIP = "localhost";
		// this.serverPort = 5555;
		this.serverIP = serverIP;
		this.serverPort = serverPort;

		threads = new ArrayList<Server_Thread>();
	}

	@Override
	public void run() {
		initialize();
	}

	private void initialize() {
		ServerSocket serverSocket = null;
		InetSocketAddress addr = null;
		Socket newSocket = null;
		InputStream is = null;
		OutputStream os = null;

		try {
			serverSocket = new ServerSocket();
			addr = new InetSocketAddress(serverIP, serverPort);
			serverSocket.bind(addr);
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Aceptando conexiones");

		do {
			try {
				newSocket = serverSocket.accept();
				System.out.println("   Nueva conexion entrante.");

				server_thread = new Server_Thread(newSocket, threads.size());
				server_thread.start();
				threads.add(server_thread);
				System.out.println("        Conexion entrante delegada.");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} while (threads.size() > 0);

		try {
			newSocket.close();
			serverSocket.close();
			System.out.println("\n\nServidor cerrado");
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("\n\nSe ha producido un error al desconectar el servidor.");
		}
	}

	public void closingThread() {
		threads_closed++;
	}
}
