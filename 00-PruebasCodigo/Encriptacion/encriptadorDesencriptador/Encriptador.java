package encriptadorDesencriptador;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class Encriptador {
	LectorFicheros lt = new LectorFicheros();
	private ArrayList<Integer> codificacion = new ArrayList<Integer>();
	private String salida = "", key = "";

	/**
	 * Este metodo sirve para encriptar el contenido indicado.
	 * 
	 * @param entrada
	 *            String de entrada a encriptar.
	 * @param rutaClave
	 *            Ruta del sistema donde esta la clave que se usara para
	 *            encriptar.
	 * @return String con el contenido encriptado.
	 */
	public String Encriptar(String entrada, String rutaClave) {
		this.key = lt.leerFichero(rutaClave);
		return EncriptadorPrivate(entrada);
	}

	/**
	 * Este metodo sirve para desencriptar el contenido indicado.
	 * 
	 * @param entrada
	 *            String de entrada a desencriptar.
	 * @param rutaClave
	 *            Ruta del sistema donde esta la clave que se usara para
	 *            desencriptar.
	 * @return String con el contenido desencriptado.
	 */
	public String Desencriptar(String entrada, String rutaClave) {
		this.key = lt.leerFichero(rutaClave);
		return DesencriptadorPrivate(entrada);
	}

	/*
	 * Este metodo se encarga de encriptar el contenido que recibe del metodo
	 * publico Encriptar.
	 */
	private String EncriptadorPrivate(String entrada) {
		char c = ' ';
		entrada = entrada.toUpperCase();
		ArrayList<Integer> posLetra = null;

		for (int i = 0; i < entrada.length(); i++) {
			c = entrada.charAt(i);
			posLetra = new ArrayList<Integer>();

			for (int j = 0; j < key.length(); j++) {
				if (c == key.charAt(j)) {
					posLetra.add(j);
				}
			}

			if (posLetra.size() != 0) {
				int posElegida = posLetra.get((int) ((Math.random() * posLetra
						.size())));
				codificacion.add(posElegida);
			}
		}

		for (Integer integer : codificacion) {
			salida = salida + integer + " ";
		}

		return salida;
	}

	/*
	 * Este metodo se encarga de desencriptar el contenido que recibe del metodo
	 * publico Desencriptar.
	 */
	private String DesencriptadorPrivate(String entrada) {
		int temp = 0;
		StringTokenizer stk = new StringTokenizer(entrada);
		while (stk.hasMoreTokens()) {
			temp = Integer.parseInt(stk.nextToken());
			for (int i = 0; i < key.length(); i++) {
				if (temp == (i + 1)) {
					salida = salida + key.charAt(i);
				}

			}
		}

		return salida;
	}
}