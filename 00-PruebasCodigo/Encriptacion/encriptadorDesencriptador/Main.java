package encriptadorDesencriptador;

import java.util.Scanner;

public class Main {
	public static void main(String args[]) {
		Encriptador encriptador = new Encriptador();
		LectorFicheros lt = new LectorFicheros();
		Scanner scan = new Scanner(System.in);
		String txt = "", rutaFichero = "", rutaClave = "", txtEncriptado = "";

		System.out.print("Ruta Fichero: ");
		rutaFichero = scan.nextLine();

		System.out.println("Texto a encriptar: ");
		txt = lt.leerFichero(rutaFichero);
		System.out.println("\n   --/ " + txt + " /--");

		System.out.print("\nRuta clave de encriptación: ");
		rutaClave = scan.nextLine();

		System.out.println("\nTexto encriptado:\n");
		txtEncriptado = encriptador.Encriptar(txt, rutaClave);
		System.out.println(txtEncriptado);
		
		rutaFichero += "-GENERADO.txt";
		
		System.out.println("Se ha generado una copia del texto en \n'"
				+ rutaFichero + "'.");
		lt.escribirFichero(rutaFichero, txtEncriptado);
		
		scan.close();
	}
	// C:\Users\david\Dropbox\0-U-Tad\Programación\0-Eclipse\workspace\00-PruebasCodigo\src\ficheros\prueba.txt
}
