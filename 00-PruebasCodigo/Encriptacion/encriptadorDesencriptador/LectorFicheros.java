package encriptadorDesencriptador;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/*
 * Esta es SOLO una clase auxiliar. Para mas información dsobre los ficheros, mirar la saeccion dedicada a los mismos.
 */

public class LectorFicheros {
	public String leerFichero(String rutaFichero) {
		File fichero = null;
		FileReader fr = null;
		BufferedReader br = null;
		String txt = " ", temp = " ";

		try {
			fichero = new File(rutaFichero);
			fr = new FileReader(fichero);
			br = new BufferedReader(fr);

			while ((temp = br.readLine()) != null) {
				txt += temp;

			}
		} catch (Exception e) {
			System.err
					.println("Se ha producido un error al leer el fichero o este no existe.");
		} finally {
			try {
				if (null != fr) {
					fr.close();
				}
			} catch (Exception e) {
				System.err
						.println("Se ha producido un error al cerrar el fichero.");
			}
		}
		return txt;
	}

	public void escribirFichero(String rutaFichero, String contenido) {
		FileWriter fichero = null;
		PrintWriter pw = null;
		try {
			fichero = new FileWriter(rutaFichero);
			pw = new PrintWriter(fichero);
			pw.println(contenido);
		} catch (Exception e) {
			System.err
					.println("Se ha producido un error al escribir en el fichero.");
		} finally {
			try {
				if (null != fichero) {
					fichero.close();
				}
			} catch (Exception e) {
				System.err
						.println("Se ha producido un error al cerrar el fichero.");
			}
		}
	}
}
