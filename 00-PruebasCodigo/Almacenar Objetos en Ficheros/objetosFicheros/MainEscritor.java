package objetosFicheros;

import java.util.ArrayList;

public class MainEscritor {
	public static void main(String args[]) {
		Almacenador al = new Almacenador();
		boolean inicioArchivo = true;
		/*
		 * Se crea un vector de objetos del tipo Persona, para almacenar en su
		 * interior todos los objetos del tipo Persona y poder trabajar con
		 * ellos mediante un, por ejemplo, for.
		 */
		ArrayList<Persona> Personas = new ArrayList<Persona>();
		String rutaFichero = "H:/Persona.txt";

		Persona p1 = new Persona("1", "Jose Manuel", "Condes",
				"C/ Rue del Percebe 13, 1�A", "555-555-555");
		// Almacenamos el objeto P�rsona en el ArrayList Personas.
		Personas.add(p1);
		Persona p2 = new Persona("2", "Miguel", "Mamolar",
				"C/ Rue del Percebe 13, 1�B", "555-555-555");
		Personas.add(p2);
		Persona p3 = new Persona("3", "Lidia", "Martin",
				"C/ Rue del Percebe 13, 2�A", "555-555-555");
		Personas.add(p3);

		for (int i = 0; i < Personas.size(); i++) {
			al.escribeFichero(rutaFichero, Personas.get(i), inicioArchivo);
			inicioArchivo = false;// Indica que ya existe una o mas personas en
									// el archivo.
		}
	}
}
