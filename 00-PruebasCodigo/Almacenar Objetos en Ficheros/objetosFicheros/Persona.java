package objetosFicheros;

import java.io.Serializable;

public class Persona implements Serializable {
	/**
	 * Se desconoce la utilidad de la siguiente linea. Ha sido generada de forma
	 * automatica por Eclipse para evitar un warning:
	 */
	private static final long serialVersionUID = 1L;

	//-------------------------------------------//

	private String IDpersona, nombre, apellidos, direccion, telefono;

	Persona(String IDpersona, String nombre, String apellidos,
			String direccion, String telefono) {
		this.IDpersona = IDpersona;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.direccion = direccion;
		this.telefono = telefono;
	}

	@Override
	public String toString() {
		return "   ----- ID: " + IDpersona + " -----\nNombre: " + nombre + " "
				+ apellidos + "\nDireccion: " + direccion + "\nTelefono: "
				+ telefono + "\n";
	}
}
