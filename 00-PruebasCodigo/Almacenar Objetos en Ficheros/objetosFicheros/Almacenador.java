package objetosFicheros;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class Almacenador {
	/**
	 * Este metodo sirve para almacenar un objeto en un fichero.
	 * 
	 * @param fichero
	 *            Ruta del fichero donde se guradaran los objetos.
	 * @param p
	 *            Objeto a guardar.
	 * @param inicioArchivo
	 *            Indica si ya existe, al menos, un objeto en ese fichero.
	 */
	public void escribeFichero(String fichero, Persona p, boolean inicioArchivo) {
		if (inicioArchivo) {// Si no existe ningun objeto en el fichero.
			try {
				ObjectOutputStream oos = new ObjectOutputStream(
						new FileOutputStream(fichero));
				oos.writeObject(p);
				oos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {// Si ya existe algun objeto en el fichero.
			try {
				// Se usa un ObjectOutputStream personalizado que no escriba una
				// cabecera en el stream.
				MiObjectOutputStream oos = new MiObjectOutputStream(
						new FileOutputStream(fichero, true));
				oos.writeUnshared(p);
				oos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Este metodo sirve para leer y mostrar los objetos almacenados en el
	 * fichero.
	 * 
	 * @param fichero
	 *            Ruta del fichero donde se guradaran los objetos.
	 */
	public void leeFichero(String fichero) {
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
					fichero));

			// Se lee el primer objeto
			Object aux = ois.readObject();
			// Mientras haya objetos
			while (aux != null) {
				if (aux instanceof Persona)
					System.out.println(aux);
				aux = ois.readObject();
			}
			ois.close();
		} catch (EOFException e1) {
			System.out.println("Fin de fichero.");
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}
}

class MiObjectOutputStream extends ObjectOutputStream {
	// Constructor que recibe OutputStream.
	public MiObjectOutputStream(OutputStream out) throws IOException {
		super(out);
	}

	// Constructor sin par�metros.
	protected MiObjectOutputStream() throws IOException, SecurityException {
		super();
	}

	// Redefinici�n del m�todo de escribir la cabecera para que no haga
	// nada.
	@Override
	protected void writeStreamHeader() throws IOException {
	}
}