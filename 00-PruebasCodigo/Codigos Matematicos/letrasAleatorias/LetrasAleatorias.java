package letrasAleatorias;

public class LetrasAleatorias {
	public static void main(String[] args) {
		/*
		 * Generamos un numero aleatorio usando Math.random, y nos aseguramos de
		 * que corresponda al valor ASCII de las letras mayusculas.
		 */
		int a = (int) (Math.random() * 26) + 65;
		// Convertimos el valor a char mediante un cast.
		char b = (char) a;
		System.out.println("La letra aleatoria es: " + b);
	}
}
