package numerosPrimos;

import java.io.BufferedReader;
import java.io.InputStreamReader;

class Metodos {
	short leerTeclado() {
		short temp = 0;
		String read = "";
		try {
			BufferedReader entrada = new BufferedReader(new InputStreamReader(
					System.in));
			read = entrada.readLine();
		} catch (Exception e) {
			System.err.println("\n\n���ATENCI�N!!!"
					+ "\n   Se ha producido un error en la lectura de datos.");
		}
		temp = Short.valueOf(read);
		return temp;
	}

	void cuentNumeros(short num) {
		int n;
		boolean ejecuta;

		for (short i = 2; i < num; i++) {
			n = 2;
			ejecuta = true;

			while (ejecuta && n < i) {
				if (i % n == 0) {
					ejecuta = false;
				} else {
					n = n + 1;
				}
			}
			if (ejecuta) {
				System.out.print(" " + i);
			}
		}
	}
}

public class NumerosPrimos {
	public static void main(String[] args) {
		Metodos m = new Metodos();
		short num = 0;

		System.out.print("Introduce un n�mero: ");
		num = m.leerTeclado();
		System.out.print("N�meros primos:");
		m.cuentNumeros(num);
	}
}